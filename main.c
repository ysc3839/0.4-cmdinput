// Main include
#include "main.h"

// Squirrel definitions
HSQAPI sq;
HSQUIRRELVM v;

// Global variables (meh)
PluginFuncs *vcmp_functions;
bool server_running = false;

static void sq_call_console_input_func(char *input_line);

#ifdef _WIN32
	#include "win.h"
#else
	#include "linux.h"
#endif

static void sq_call_console_input_func(char *input_line)
{
	// Save the current stack position of the VM
	SQInteger top = sq->gettop(v);

	// Push the root table
	sq->pushroottable(v);

	// Get the field "onConsoleInput" from the root table
	sq->pushstring(v, _SC("onConsoleInput"), -1);

	// Get the function "onConsoleInput" itself from the root table if we can
	if (SQ_SUCCEEDED(sq->get(v, -2)))
	{
		SQChar *text = input_line;
		SQChar *space_pos = strchr(text, ' ');

		// Push "this" which is the root table since onConsoleInput is
		// expected to be a global function.
		sq->pushroottable(v);

		// Push our string arguments
		sq->pushstring(v, _SC(text), -1);
		if (space_pos)
		{
			space_pos[0] = '\0';
			sq->pushstring(v, _SC(&space_pos[1]), -1);
		}
		else
			sq->pushnull(v);

		// Call the function
		sq->call(v, 3, 0, 1);
	}

	// Restore the stack
	sq->settop(v, top);
}

static void vcmp_on_squirrel_script_load()
{
	// See if we have any imports from Squirrel
	size_t size;
	int32_t sq_id = vcmp_functions->FindPlugin("SQHost2");
	if (sq_id != -1) // vcmpEntityNone
	{
		const void **sq_exports = vcmp_functions->GetPluginExports(sq_id, &size);

		// We do!
		if (sq_exports != NULL && size > 0)
		{
			// Cast to a SquirrelImports structure
			SquirrelImports **sq_deref_funcs = (SquirrelImports **)sq_exports;

			// Now let's change that to a SquirrelImports pointer
			SquirrelImports *sq_funcs = (SquirrelImports *)(*sq_deref_funcs);

			// Now we get the virtual machine
			if (sq_funcs)
			{
				// Get a pointer to the VM and API
				sq = *(sq_funcs->GetSquirrelAPI());
				v = *(sq_funcs->GetSquirrelVM());

				os_init_console_input();
			}
		}
		else
			output_console_message("Failed to attach to SQHost2.");
	}
}

static uint8_t vcmp_on_server_initialise(void)
{
	#define _STR(s) #s
	#define STR(s) _STR(s)
	output_console_message("Loaded CmdInput plugin for SqVCMP by ysc3839. (plugin api: " STR(PLUGIN_API_MAJOR) "." STR(PLUGIN_API_MINOR) ")");
	return true;
}

static void vcmp_on_server_shutdown(void)
{
	server_running = false;
	os_uninit_console_input();
}

static uint8_t vcmp_on_plugin_command(uint32_t command_identifier, const char *message)
{
	switch (command_identifier)
	{
	case 0x7D6E22D8:
		vcmp_on_squirrel_script_load();
		break;
	}
	return true;
}

EXPORT unsigned int VcmpPluginInit(PluginFuncs *functions, PluginCallbacks *callbacks, PluginInfo *info)
{
	// Set our plugin information
	info->pluginVersion = 0x1320; // 1.3.2.0
	strncpy(info->name, "CmdInput", sizeof(info->name));

	info->apiMajorVersion = PLUGIN_API_MAJOR;
	info->apiMinorVersion = PLUGIN_API_MINOR;

	// Store functions for later use
	vcmp_functions = functions;

	// Store callback
	callbacks->OnServerInitialise = vcmp_on_server_initialise;
	callbacks->OnServerShutdown = vcmp_on_server_shutdown;
	callbacks->OnServerFrame = vcmp_on_server_frame;
	callbacks->OnPluginCommand = vcmp_on_plugin_command;

	// Done!
	return 1;
}
