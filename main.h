#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#ifdef _WIN32
	#define WIN32_LEAN_AND_MEAN
	#include <Windows.h>
	#include <process.h>

	#define EXPORT __declspec(dllexport)

	#define strncpy(dst, src, n) strcpy_s(dst, n, src)
#else
	#define EXPORT

	// Standard readline include files.
	#include <readline/readline.h>
	#include <readline/history.h>

	// Used for select(2)
	#include <sys/types.h>
	#include <sys/select.h>

	#include <signal.h>
	#include <errno.h>
#endif

#include "VCMP.h"

// Squirrel
#include "SqImports.h"

#include "console.h"
