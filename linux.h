bool sigwinch_received = false;

// Handle SIGWINCH and window size changes when readline is not active and reading a character.
static void sighandler(int sig)
{
	sigwinch_received = true;
}

// Callback function called for each line when accept-line executed, EOF seen, or EOF character read.
static void readline_handler(char *input_line)
{
	if (input_line != NULL)
	{
		// Add to history if line is not empty.
		if (*input_line)
			add_history(input_line);

		sq_call_console_input_func(input_line);

		free(input_line);
	}
}

static void os_init_console_input()
{
	// Handle window size changes when readline is not active and reading characters.
	signal(SIGWINCH, sighandler);

	// Install the line handler.
	rl_callback_handler_install(NULL, readline_handler);

	server_running = true;
}

static void os_uninit_console_input()
{
	rl_callback_handler_remove();
}

static void vcmp_on_server_frame(float elapsed_time)
{
	if (server_running)
	{
		fd_set fds;
		struct timeval timeout;

		FD_ZERO(&fds);
		FD_SET(fileno(rl_instream), &fds);

		timeout.tv_sec = 0;
		timeout.tv_usec = 0;

		int r = select(FD_SETSIZE, &fds, NULL, NULL, &timeout);
		if (r < 0 && errno != EINTR)
		{
			server_running = false;
			output_console_message("select error.");
			rl_callback_handler_remove();
			return;
		}
		if (sigwinch_received)
		{
			sigwinch_received = false;
			rl_resize_terminal();
		}
		if (r < 0)
			return;

		if (FD_ISSET(fileno(rl_instream), &fds))
			rl_callback_read_char();
	}
}
