LONG inputed = FALSE;
char input_line[2048];

static void win_console_input_thread(void *p)
{
	while (server_running)
	{
		static char _input_line[2048];
		if (fgets(_input_line, sizeof(_input_line), stdin) != NULL)
		{
			_input_line[strcspn(_input_line, "\n")] = 0; // Remove trailing newline character. https://stackoverflow.com/a/28462221
			strcpy_s(input_line, sizeof(_input_line), _input_line);
			InterlockedExchange(&inputed, TRUE); // inputed = TRUE;
		}
	}
}

static void os_init_console_input()
{
	server_running = true;
	_beginthread(win_console_input_thread, 0, NULL);
}

static void os_uninit_console_input()
{
}

static void vcmp_on_server_frame(float elapsed_time)
{
	if (server_running)
	{
		// inputed_old = inputed; if (inputed == TRUE) inputed = FALSE; return inputed_old;
		if (InterlockedCompareExchange(&inputed, FALSE, TRUE) == TRUE)
		{
			sq_call_console_input_func(input_line);
		}
	}
}
